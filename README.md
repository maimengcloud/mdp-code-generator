# 唛盟-mdp代码生成器[mdp-code-generator](https://gitee.com/qingqinkj/mdp-code-generator) 
基于数据库表结构创建增删改查代码，把前端代码创建到前端项目中，把后端代码创建到后端项目中  
⚠️注意：默认生成后的代码运行环境依赖  elementui,mybatis plus,spring boot,
[mdp-core](https://gitee.com/qingqinkj/mdp-core) ,
[mdp-ui](https://gitee.com/qingqinkj/mdp-sys-ui-web)
如果不想依赖mdp相关代码，可以下载代码生成器工程[mdp-dev](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-dev) 进行修改代码模板
## ⚙ 快速开始

### 下载安装
```bash
# 克隆项目
git clone https://gitee.com/qingqinkj/mdp-code-generator.git

# 安装依赖 导入工程到开发工具中，将工程转为maven工程
```
### 配置数据库链接
>⚠️注意：生成代码完全依赖数据库表结构，表结构必须设置主键、尽量设置表描述、字段描述
- 找到[application-dev.yml](src/main/resources/application-dev.yml)
- 找到spring.datasource.url、spring.datasource.username、spring.datasource.password进行配置

### 生成代码

- 找到 [TestCodegenService.java](src/test/java/com/mdp/dev/TestCodegenService.java)中的函数createAll_pkg
- 输入 dbOwner,javaPackage,tableNames,
- 执行用例函数 createAll_pkg     参考以下代码：
```java 
// 数据库拥有者
String dbOwner="adm";

// 后端代码存放工程
String serviceProjectPath="D:/IdeaProjects/mdp-sys-backend/mdp-sys";

// 前端代码存放工程
String viewProjectPath="D:/IdeaProjects/mdp-sys-ui-web";

// 生成的java代码文件存放的java包目录
String javaPackage="com.mdp.mo";

// 生成代码文件名需要过滤掉的前缀 比如不想mo_出现在文件名，可以填ignoePrefixs="mo_"
String ignoePrefixs="";

// 如果文件名已存在，是否覆盖
boolean isForceOveride=true;

// 表名列表，可以填入任意多个表
String[] tableNames={"mo_order","mo_order_module"};
super.createAll(dbOwner, serviceProjectPath, viewProjectPath, javaPackage, ignoePrefixs, isForceOveride, tableNames);
```

# 持续改进
再不断实践中，根据企业特点，页面功能及布局需要调整，可以修改模板达到目的
## 修改代码模板
[mdp-dev](https://gitee.com/qingqinkj/mdp-core/tree/master/mdp-dev) 代码生成器工程
- 前端相关模板  
  [vueForm.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/vueForm.ftl) 表单页面,
  [vueMng.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/vueMng.ftl) 综合管理页面,
  [viewApi.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/viewApi.ftl) api文件
- 后端相关模板  
  [crudController.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/crudController.ftl) 控制层类,
  [crudService.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/crudService.ftl) 服务类,
  [subEntity.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/subEntity.ftl) 实体类,
  [myBatisMapper.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/myBatisMapper.ftl) MyBatisMapper.xml,
  [myBatisMapperJava.ftl](https://gitee.com/qingqinkj/mdp-core/blob/master/mdp-dev/src/main/resources/templates/dev/myBatisMapperJava.ftl) MyBatisMapper.java
 