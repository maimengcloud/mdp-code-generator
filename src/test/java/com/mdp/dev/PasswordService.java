package com.mdp.dev;


import com.mdp.dev.utils.SafePasswordEncoder;

/**
 *  @author cyc 20150820
 *  
 ****/
public class PasswordService {

	SafePasswordEncoder safePasswordEncoder=new SafePasswordEncoder();
	public PasswordService(){

	}

	/**
	 * 重置超级管理员密码
	 * @return
	 */
	public String createPassword(String rawPassword){
		return safePasswordEncoder.encode(rawPassword);
	}

	public static void main(String[] args) {
		PasswordService passwordService=new PasswordService();
		String rawPassword="e3ceb5881a0a1fdaad01296d7554868d";
		String password="$2a$10$2jUTJTqBJgWGW.itihYWr.OWt4RGDHWIHAW/K3c6Dau//1h4RuAyG";
		System.out.printf(" 密码：%s ", password);
		boolean isMatch=passwordService.safePasswordEncoder.matches(rawPassword,password);
		System.out.printf(" 密码是否匹配： %s ", isMatch);

	}
}
