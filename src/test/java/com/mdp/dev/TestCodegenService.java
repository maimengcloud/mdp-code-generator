package com.mdp.dev;

import com.mdp.dev.main.CodeBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 调用代码生成器创建前后端代码
 * @author cyc
 * @since 20150820
 ****/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestCodegenService extends CodeBase {

/**
 * 创建代码
 * @param dbOwner 数据库owner
 * @param serviceProjectPath 服务项目所在目录
 * @param viewProjectPath 页面项目所在目录
 * @param javaPackage java src 下的包结构 如javaPackage=com.mdp.sys ,将会在该包下创建子包${tableName},驼峰命名,代码存放在 com.mdp.sys.${tableName}的ctrl/entity/mapper/service四个目录
 * @param pagePathFilter 根据pagePathFilter的正则表达式，去掉部分javaPackage的前缀作为页面存放目录
 * @param ignoePrefixs,生成代码时，命名文件时需要忽略的前缀，多个以逗号分隔
 * @param isForceOveride 生成文件时，如果目录下已有同名文件是否覆盖
 * @param printTableField  是否打印@TableField到实体类中
 * @param tableNames 依赖的表，多个以逗号分隔
 */
	/**
	 * 例子
	 */
	//@Test
	public void createAll_demo(){
		// 数据库拥有者
		String dbOwner="adm";

		// 后端代码存放工程
		String serviceProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\oa-backend\\oa";

		// 前端代码存放工程
		String viewProjectPath="D:/IdeaProjects/oa-ui-web";

		// java src 下的包结构 如javaPackage=com.mdp.sys ,将会在该包下创建子包${tableName},驼峰命名,代码存放在 com.mdp.sys.${tableName}的ctrl/entity/mapper/service四个目录
		String javaPackage="com.oa.car";

		// 根据pagePathFilter的正则表达式，去掉部分javaPackage的前缀作为页面存放目录
		String pathFilter="com.";

		// 生成代码时，命名文件时需要忽略的前缀，多个以逗号分隔
		String ignoePrefixs="";

		// 如果文件名已存在，是否覆盖
		boolean isForceOveride=true;

		// 是否在实体属性上打印 @TableField注解，当字段命名不规范时，设为true
		boolean printTableField=false;

		// 表名列表，可以填入任意多个表
		String[] tableNames={"mo_order","mo_order_module"};
		super.createAll(dbOwner, serviceProjectPath, viewProjectPath, javaPackage,pathFilter, ignoePrefixs, isForceOveride,printTableField, tableNames);
	}
	@Test
	public void createAll_xm() {
		String dbOwner="xm";
		String serviceProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\xm-backend\\xm-core";
		String viewProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\xm-ui-web";
		String javaPackage="com.xm.core";
		String pathFilter="com.";
		String ignoePrefixs="xm_";
		boolean isForceOveride=true;
		boolean printTableField=false;

		String[] tableNames={"xm_branch_state","xm_branch_state_his","xm_branch_task_type_state","xm_budget_labor","xm_budget_nlabor","xm_collect","xm_collect_link","xm_cost_nlabor",
				"xm_env_list","xm_func","xm_iteration","xm_iteration_state","xm_iteration_state_his","xm_menu","xm_menu_comment","xm_menu_exchange",
				"xm_menu_state","xm_my_focus","xm_my_foot_print","xm_product","xm_product_project_link","xm_product_state","xm_product_state_his","xm_product_version",
				"xm_project","xm_project_env_list","xm_project_kpi","xm_project_kpi_his","xm_project_state","xm_project_state_his","xm_question",
				"xm_question_handle","xm_question_workload","xm_record","xm_rpt_config","xm_rpt_data","xm_task","xm_task_bid_order","xm_task_comment",
				"xm_task_eval","xm_task_execuser","xm_task_order","xm_task_sbill","xm_task_sbill_detail","xm_task_skill","xm_test_case","xm_test_casedb",
				"xm_test_plan","xm_test_plan_case","xm_workload"
		};

		super.createAll(dbOwner, serviceProjectPath, viewProjectPath, javaPackage, pathFilter,ignoePrefixs, isForceOveride,printTableField, tableNames);
	}

	@Test
	public void createAll_form() {
		String dbOwner="wflow";
		String serviceProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\mdp-workflow-backend\\mdp-workflow";
		String viewProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\mdp-lcode-ui-web11";
		String javaPackage="com.mdp.workflow.biz.ru";
		String pathFilter="com.";
		String ignoePrefixs="act_ru_";
		boolean isForceOveride=false;
		boolean printTableField=false;

		String[] tableNames={"act_ru_actinst"};

		super.createAll(dbOwner, serviceProjectPath, viewProjectPath, javaPackage, pathFilter,ignoePrefixs, isForceOveride,printTableField, tableNames);
	}
	@Test
	public void createAll_pkg() {
		String dbOwner="xm";
		String serviceProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\xm-backend\\xm-core";
		String viewProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\xm-ui-webxxxxxxxxxxx";
		String javaPackage="com.xm.core";
		String pathFilter="com.";
		String ignoePrefixs="";
		boolean isForceOveride=false;
 		boolean printTableField=false;
		String[] tableNames={ "xm_group","xm_group_user","xm_group_state"
		};

		super.createAll(dbOwner, serviceProjectPath, viewProjectPath, javaPackage, pathFilter,ignoePrefixs, isForceOveride,printTableField, tableNames);
	}


	@Test
	public void createAll_dm() {
		String dbOwner="adm";
		String serviceProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\mdp-dm-backend\\mdp-dm";
		String viewProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\mdp-lcode-ui-web";
		String javaPackage="com.mdp.dm";
		String pathFilter="com.";
		String ignoePrefixs="dm_";
		boolean isForceOveride=false;
		boolean printTableField=false;
		String[] tableNames={ "dm_model"
		};

		super.createAll(dbOwner, serviceProjectPath, viewProjectPath, javaPackage, pathFilter,ignoePrefixs, isForceOveride,printTableField, tableNames);
	}

	@Test
	public void createAll_sms() {
		String dbOwner="sms";
		String serviceProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\mdp-sms-backend\\mdp-sms";
		String viewProjectPath="C:\\Users\\陈裕财\\IdeaProjects\\mdp-sms-ui-web";
		String javaPackage="com.mdp.sms";
		String pathFilter="com.";
		String ignoePrefixs="";
		boolean isForceOveride=true;
		boolean printTableField=false;
		String[] tableNames={ "sms_flow","sms_reply","sms_service_charges","sms_service_charges_flow","sms_sign","sms_template"
		};

		super.createAll(dbOwner, serviceProjectPath, viewProjectPath, javaPackage, pathFilter,ignoePrefixs, isForceOveride,printTableField, tableNames);
	}
}
