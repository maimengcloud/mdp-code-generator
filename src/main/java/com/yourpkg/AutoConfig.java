package com.yourpkg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 要使用mdp平台功能，必须 扫码com.mdp包
 * 一些默认公共配置
 */
@ComponentScan(basePackages={"com.mdp"})
@Configuration
public class AutoConfig {


}
